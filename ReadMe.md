Tray Tools - Allows develpers to setup tasks that they want easy access to.

> # LICENSE
> MIT License- see LICENSE

# INFO
* Create a list of processes to start or stop
  * Start/Stop them individually
  * Start/Stop them all
* Copy the contents of one folder to another, replacing the content.
  * Useful for moving debug folders.


# Release 0.0.0.2
* Changes
  * Initial Project Setup
* Downloads
  * [JoshuaElla.TrayTools.0.0.0.2.zip](/releases/JoshuaElla.TrayTools.0.0.0.2.zip)


# Structure
```bash
$ project
.
├── CommonAssemblyInfo.cs
├── Core
│   ├── Core.csproj
│   ├── Events
│   │   └── SettingsChangedEvent.cs
│   ├── Models
│   │   ├── MenuItem.cs
│   │   └── Settings.cs
│   ├── packages.config
│   ├── Properties
│   │   └── AssemblyInfo.cs
│   └── Services
│       ├── IProcessService.cs
│       ├── ISettingsService.cs
│       ├── ISyncService.cs
│       ├── ProcessService.cs
│       ├── SettingsService.cs
│       └── SyncService.cs
└── SystemTray
    ├── App.config
    ├── App.xaml
    ├── App.xaml.cs
    ├── Commands
    │   └── DelegateCommand.cs
    ├── Controls
    │   └── ScrollingTextBox.cs
    ├── Converters
    │   └── StringCollectionToTextConverter.cs
    ├── IMainViewModel.cs
    ├── je.ico
    ├── MainWindowViewModel.cs
    ├── MainWindow.xaml
    ├── MainWindow.xaml.cs
    ├── NotifyIconResources.xaml
    ├── packages.config
    ├── Properties
    │   ├── AssemblyInfo.cs
    │   ├── Resources.Designer.cs
    │   └── Resources.resx
    ├── Resources
    │   ├── green.ico
    │   ├── red.ico
    │   └── yellow.ico
    ├── Settings.json  # Settings File (Adjust this)
    ├── SystemTray.csproj
    └── UserControls
        ├── SettingsViewModel.cs
        ├── SettingsWindow.xaml
        ├── SettingsWindow.xaml.cs
        ├── SyncWindowViewModel.cs
        ├── SyncWindow.xaml
        └── SyncWindow.xaml.cs

```

## Getting started
1. Change your Settings
```json
{
  "DebugPath": "C:/projects/build/bin/Debug", // Folder you want to copy
  "StagePath": "c:/temp/stage",               // Folder you want to copy to
  "Instances": [                              // Applications that you want to run 
    {
      "Name": "MyApp",
      "Path": "c:/path/to/exe/directory",
      "Exe": "myApp.exe",
      "Args": "-x -a"
    },
    {
      "Name": "MyOtherApp",
      "Path": "c:/path/to/exe/directory",
      "Exe": "myApp.exe",
      "Args": "-x -a"
    }
  ]
}
```
1. Build the solution
2. Go to the build directory
3. Copy the content to a path of your choice

```bash
$ somepath> notepad Settings.json # Change your settings
$ somepath> JoshuaElla.SystemTray.exe
```
# REQUIREMENTS

To run the build, you will require .net 4.6.2 and an IDE capable of building.

# CREDITS
Joshua Ella Ltd - A software consultany   
Copyright 2019 Joshua Ella Ltd. All rights reserved
