﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JoshuaElla.SystemTray.Core.Models;

namespace JoshuaElla.SystemTray.Core.Services
{
    public interface IProcessService
    {
        ObservableCollection<MenuItem> MenuItems { get; set; }

        void StartProcess(string name);

        void StartAllProcesses();

        void StopProcess(string name);

        void StopAllProcesses();

        void SyncProcesses(List<Instance> instances);
    }
}