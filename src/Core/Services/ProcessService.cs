﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using JoshuaElla.SystemTray.Core.Models;

namespace JoshuaElla.SystemTray.Core.Services
{
    public class ProcessService : IProcessService
    {
        private readonly ISettingsService _settingsService;
        private List<Instance> _instances = new List<Instance>();

        public ProcessService(ISettingsService settingsService)
        {
            settingsService.SettingsChanged += OnSettingsChanged;
            MenuItems = new ObservableCollection<MenuItem>();
            OnSettingsChanged(settingsService.ActiveSettings);
        }

        public ObservableCollection<MenuItem> MenuItems { get; set; }

        public void StartProcess(string name)
        {
            var menuItem = MenuItems.First(i => i.Name == name);
            var instance = _instances.First(i => i.Name == name);

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = instance.Exe,
                    Arguments = instance.Args,
                    WindowStyle = ProcessWindowStyle.Normal,
                    WorkingDirectory = instance.Path
                },
                EnableRaisingEvents = true
            };

            process.Exited += ProcessStopped;
            try
            {
                var started = process.Start();

                menuItem.ProcessId = process.Id;
                menuItem.Started = started;
            }
            catch
            {
                process.Exited -= ProcessStopped;
            }
            
        }

        public void StartAllProcesses()
        {
            foreach (var item in MenuItems.Where(m => ! m.Started))
            {
                StartProcess(item.Name);
            }
        }

        public void StopProcess(string name)
        {
            var menuItem = MenuItems.FirstOrDefault(i => i.Name == name && i.Started);
            if (null == menuItem)
            {
                return;
            }

            Process.GetProcessById(menuItem.ProcessId).Kill();
        }

        public void StopAllProcesses()
        {
            foreach (var item in MenuItems.Where(m => m.Started))
            {
                StopProcess(item.Name);
            }
        }

        private void ProcessStopped(object sender, EventArgs e)
        {
            var process = (Process) sender;
            var menuItem = MenuItems.FirstOrDefault(m => m.ProcessId == process.Id);
            if (null == menuItem)
            {
                return;
            }

            menuItem.Started = false;
        }

        public void SyncProcesses(List<Instance> newInstances)
        {

            var removable = _instances.Except(newInstances);
            foreach (var menuItem in removable)
            {
                StopProcess(menuItem.Name);
                var oldItem = MenuItems.FirstOrDefault(i => i.Name == menuItem.Name);
                MenuItems.Remove(oldItem);
            }

            var addable = newInstances.Except(_instances);
            foreach (var menuItem in addable)
            {
                MenuItems.Add(new MenuItem
                {
                    Name = menuItem.Name,
                    Started = false
                });
            }

            _instances = newInstances;
        }

        private void OnSettingsChanged(Settings settings)
        {
            SyncProcesses(settings.Instances);
        }
    }
}
