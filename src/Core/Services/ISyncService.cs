﻿
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace JoshuaElla.SystemTray.Core.Services
{
    public interface ISyncService
    {
        Task SyncFolder(ObservableCollection<string> messages);
    }
}
