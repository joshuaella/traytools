﻿using System;
using System.IO;
using JoshuaElla.SystemTray.Core.Models;
using Newtonsoft.Json;

namespace JoshuaElla.SystemTray.Core.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly string _settingsFile = "Settings.json";

        public SettingsService()
        {
            LoadSettings();
        }

        public event Action<Settings> SettingsChanged;

        public string Settings => GetSettingsFromFile();

        public Settings ActiveSettings { get; set; }

        public void LoadSettings()
        {
            var newSettings = File.Exists(_settingsFile) 
                ? DeserializeSettings(File.ReadAllText(_settingsFile)) 
                : null;

            ApplySettings(newSettings);
        }

        public void LoadSettings(string settings)
        {
            try
            {
                var newSettings = DeserializeSettings(settings);
                ApplySettings(newSettings);
            }
            catch
            {

            }
        }

        public Settings DeserializeSettings(string settings)
        {
            return JsonConvert.DeserializeObject<Settings>(settings);
        }

        private string GetSettingsFromFile()
        {
            return File.Exists(_settingsFile) ? File.ReadAllText(_settingsFile) : string.Empty;
        }

        private void ApplySettings(Settings newSettings)
        {
            if (newSettings == ActiveSettings)
            {
                return;
            }

            ActiveSettings = newSettings;
            SettingsChanged?.Invoke(ActiveSettings);
            File.Delete(_settingsFile);
            File.WriteAllText(_settingsFile, JsonConvert.SerializeObject(ActiveSettings, Formatting.Indented));
        }

    }
}
