﻿using System;
using JoshuaElla.SystemTray.Core.Models;

namespace JoshuaElla.SystemTray.Core.Services
{
    public interface ISettingsService
    {
        event Action<Settings> SettingsChanged;
        string Settings { get; }
        Settings DeserializeSettings(string settings);
        Settings ActiveSettings { get; set; }
        void LoadSettings();
        void LoadSettings(string settings);
    }
}