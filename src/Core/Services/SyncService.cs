﻿using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;

namespace JoshuaElla.SystemTray.Core.Services
{
    public class SyncService : ISyncService
    {
        private readonly ISettingsService _settingsService;

        public SyncService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public async Task SyncFolder(ObservableCollection<string> messages)
        {
            var from = _settingsService.ActiveSettings.DebugPath;
            var to = _settingsService.ActiveSettings.StagePath;

            if (!Directory.Exists(from))
            {
                messages.Add($"Could not find debug folder : '{from}'");
                return;
            }
            await Task.Run(() =>
            {
                RecursiveDelete(new DirectoryInfo(to), messages);

                if (!Directory.Exists(to))
                {
                    Directory.CreateDirectory(to);
                }

                //Now Create all of the directories
                foreach (string dirPath in Directory.GetDirectories(from, "*",
                    SearchOption.AllDirectories))
                {
                    var path = dirPath.Replace(from, to);
                    messages.Add($"Creating Directory : '{path}'");
                    Directory.CreateDirectory(path);
                }
                    

                //Copy all the files & Replaces any files with the same name
                foreach (string oldPath in Directory.GetFiles(from, "*.*",
                    SearchOption.AllDirectories))
                {
                    var newPath = oldPath.Replace(from, to);
                    messages.Add($"Coping File: '{oldPath}' to '{newPath}'");
                    File.Copy(oldPath, newPath, true);
                }
            });
            messages.Add($"Done.");
        }

        private void RecursiveDelete(DirectoryInfo directory, ObservableCollection<string> messages)
        {
            if (!directory.Exists)
            {
                return;
            }
                
            foreach (var dir in directory.EnumerateDirectories())
            {
                RecursiveDelete(dir, messages);
            }
            messages.Add($"Deleting: {directory.FullName}");
            directory.Delete(true);
        }
    }
}
