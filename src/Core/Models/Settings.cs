﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JoshuaElla.SystemTray.Core.Models
{
    public class Instance
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Path")]
        public string Path { get; set; }

        [JsonProperty("Exe")]
        public string Exe { get; set; }

        [JsonProperty("Args")]
        public string Args { get; set; }
    }

    public class Settings
    {
        public Settings()
        {
            Instances = new List<Instance>();
        }
        
        [JsonProperty("DebugPath")]
        public string DebugPath { get; set; }

        [JsonProperty("StagePath")]
        public string StagePath { get; set; }

        [JsonProperty("Instances")]
        public List<Instance> Instances { get; set; }
    }

    
}
