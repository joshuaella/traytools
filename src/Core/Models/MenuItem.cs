﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using JoshuaElla.SystemTray.Core.Annotations;

namespace JoshuaElla.SystemTray.Core.Models
{
    public class MenuItem : INotifyPropertyChanged
    {
        private int _processId;
        private string _name;
        private bool _started;

        public int ProcessId
        {
            get => _processId;
            set
            {
                _processId = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public bool Started
        {
            get => _started;
            set
            {
                _started = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
