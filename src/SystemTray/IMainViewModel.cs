﻿using System.Windows.Input;

namespace JoshuaElla.SystemTray
{
    public interface IMainWindowViewModel
    {
        ICommand ShowDialogCommand { get; }
    }
}