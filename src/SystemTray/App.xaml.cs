﻿using System.Windows;
using JoshuaElla.SystemTray.Core.Services;
using JoshuaElla.SystemTray.UserControls;
using Prism.Events;
using Prism.Ioc;

namespace JoshuaElla.SystemTray
{
    public partial class App
    {
        protected override void RegisterTypes(IContainerRegistry container)
        {
            // Prism
            container.RegisterSingleton<IEventAggregator, EventAggregator>();

            // Services
            container.RegisterSingleton<IProcessService, ProcessService>()
                .RegisterSingleton<ISettingsService, SettingsService>()
                .RegisterSingleton<ISyncService, SyncService>();

            // Dialogs
            container.RegisterDialog<SyncWindow, SyncWindowViewModel>();
            container.RegisterDialog<SettingsWindow, SettingsViewModel>();

            // MainWindow
            container.RegisterSingleton<IMainWindowViewModel, MainWindowViewModel>();
            container.Register<MainWindow>();
        }

        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }
    }
}