﻿using System.Windows.Input;
using JoshuaElla.SystemTray.Core.Models;
using JoshuaElla.SystemTray.Core.Services;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace JoshuaElla.SystemTray
{
    public class MainWindowViewModel : BindableBase, IMainWindowViewModel
    {
        private readonly ICommand _showDialogCommand;
        private ICommand _processCommand;
        private ICommand _processAllCommand;

        private readonly IDialogService _dialogService;
        private IProcessService _processService;

        public MainWindowViewModel(
            IProcessService processService, 
            IDialogService dialogService)
        {
            ProcessService = processService;
            _dialogService = dialogService;
            _showDialogCommand = new Commands.DelegateCommand(ShowDialog);
            _processCommand = new Commands.DelegateCommand(ToggleProcess);
            _processAllCommand = new Commands.DelegateCommand(ProcessAll);
        }

        public string Title { get; set; }

        public ICommand ShowDialogCommand
        {
            get { return _showDialogCommand; }
        }

        public ICommand ProcessCommand
        {
            get { return _processCommand; }
        }

        public ICommand ProcessAllCommand
        {
            get { return _processAllCommand; }
        }

        public IProcessService ProcessService
        {
            get => _processService;
            set { SetProperty(ref _processService, value); }
        }

        private void ShowDialog(object obj)
        {
            _dialogService.ShowDialog(obj.ToString(), null, null);
        }

        private void ToggleProcess(object menuItem)
        {
            var item = menuItem as MenuItem;
            if (item.Started)
            {
                _processService.StopProcess(item.Name);
            }
            else
            {
                _processService.StartProcess(item.Name);
            }
        }

        private void ProcessAll(object startStop)
        {
            var action = startStop.ToString().ToLower().Trim();
            if (action == "start")
            {
                _processService.StartAllProcesses();
            }
            else
            {
                _processService.StopAllProcesses();
            }
        }

    }
}
