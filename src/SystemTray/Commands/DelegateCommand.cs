﻿using System;
using System.Windows.Input;

namespace JoshuaElla.SystemTray.Commands
{
    public class DelegateCommand : ICommand
    {
        private Action<object> _action;
        private readonly bool _canExecute;

        public DelegateCommand(Action<object> action, bool canExecute = true)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _action(parameter);
        }
    }
}