﻿namespace JoshuaElla.SystemTray
{
    public partial class MainWindow
    {
        public MainWindow(IMainWindowViewModel mainViewModel)
        {
            DataContext = mainViewModel;
            InitializeComponent();
        }
    }
}