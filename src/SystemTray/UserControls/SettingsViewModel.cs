﻿using System;
using System.Linq;
using System.Windows.Input;
using JoshuaElla.SystemTray.Core.Services;
using Prism.Services.Dialogs;

namespace JoshuaElla.SystemTray.UserControls
{
    public class SettingsViewModel : DialogViewModelBase
    {
        private readonly ISettingsService _settingsService;
        private string _status;
        private string _message;
        private string _settings;

        public SettingsViewModel(ISettingsService settingsService)
        {
            Title = "Settings Window";
            _settingsService = settingsService;
            SaveCommand = new Prism.Commands.DelegateCommand(SaveSettings);
            CloseDialogCommand = new Prism.Commands.DelegateCommand(CloseDialog);
        }

        public ICommand CloseDialogCommand { get; }
        public ICommand SaveCommand { get; }

        public string Status
        {
            get => _status;
            set { SetProperty(ref _status, value); }
        }

        public string Message
        {
            get => _message;
            set { SetProperty(ref _message, value); }
        }

        public string Settings
        {
            get { return _settings; }
            set { SetProperty(ref _settings, value); }
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            Settings = _settingsService.Settings;
            Status = "Loaded";
            Message = string.Empty;
        }

        private void SaveSettings()
        {
            Status = "Saving";
            Message = string.Empty;
            try
            {
                var settings = _settingsService.DeserializeSettings(Settings);
                var groups = settings.Instances.GroupBy(i => i.Name).Where(g => g.Count() > 1);
                if (groups.Any())
                {
                    var lines = Settings.Split(new[] {Environment.NewLine}, StringSplitOptions.None).Reverse().ToList();
                    var totalLines = lines.Count;
                    foreach (var line in lines)
                    {
                        if (line.Contains($"\"{groups.First().Select(g => g.Name)}\""))
                        {
                            break;
                        }

                        totalLines--;

                    }

                    Status = "Save Failed";
                    Message = $"Duplicate Name on Line {totalLines}";
                }

                _settingsService.LoadSettings(Settings);
                Status = "Saved Settings";
                Message = string.Empty;
                Settings = _settingsService.Settings;
            }
            catch (Exception ex)
            {
                Status = "Error";
                Message = ex.Message;
            }
        }

        private void CloseDialog()
        {
            RaiseRequestClose(new DialogResult(true));
            Status = null;
            Message = null;
            Settings = null;
        }
    }
}
