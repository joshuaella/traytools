﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using JoshuaElla.SystemTray.Core.Services;
using Prism.Services.Dialogs;

namespace JoshuaElla.SystemTray.UserControls
{
    public class SyncWindowViewModel : DialogViewModelBase
    {
        private readonly ISettingsService _settingsService;
        private readonly ISyncService _syncService;
        private string _status;
        private string _source;
        private string _destination;
        private ObservableCollection<string> _messages;

        public SyncWindowViewModel(
            ISettingsService settingsService, 
            ISyncService syncService)
        {
            _settingsService = settingsService;
            _syncService = syncService;
            Title = "Sync Window";
            Messages = new ObservableCollection<string>();
            SaveCommand = new Prism.Commands.DelegateCommand(async () => Process());
            CloseDialogCommand = new Prism.Commands.DelegateCommand(CloseDialog);
        }

        public ICommand CloseDialogCommand { get;  }
        public ICommand SaveCommand { get; }

        public ObservableCollection<string> Messages
        {
            get => _messages;
            set { SetProperty(ref _messages, value); }
        }

        public string Source
        {
            get { return _source; }
            set { SetProperty(ref _source, value); }
        }

        public string Destination
        {
            get { return _destination; }
            set { SetProperty(ref _destination, value); }
        }

        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        private async Task Process()
        {
            Messages = new ObservableCollection<string>();
            await _syncService.SyncFolder(Messages);
        }

        private void CloseDialog()
        {
            RaiseRequestClose(new DialogResult(true));
            Source = null;
            Destination = null;
            Messages = null;
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            Source = _settingsService.ActiveSettings.DebugPath;
            Destination = _settingsService.ActiveSettings.StagePath;
            Messages = new ObservableCollection<string>();
        }
    }
}
