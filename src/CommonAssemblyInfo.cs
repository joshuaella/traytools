using System.Reflection;

[assembly: AssemblyCompany("Joshua Ella Ltd")]
[assembly: AssemblyProduct("SystemTray")]
[assembly: AssemblyDescription("System Tray for Tasks")]
[assembly: AssemblyCopyright("Copyright \u00a9 JoshuaElla 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.0.0.3")]
[assembly: AssemblyFileVersion("0.0.0.3")]
